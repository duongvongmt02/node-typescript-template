import { ObjectSchema } from 'joi';

export type TReqField = 'query' | 'params' | 'body';
export type TReqArgs = {
  [key: string]: any;
  params?: object;
  query?: object;
  body?: object;
};
export interface IValidatorSchema<T> {
  [key: string]: any;
  params?: ObjectSchema<T>;
  query?: ObjectSchema<T>;
  body?: ObjectSchema<T>;
}
