import Joi from 'joi';

export const stringSchema = Joi.string();

export const pageSizeSchema = Joi.number().min(1).max(50);
export const pageSchema = Joi.number().min(1);
export const pathIdSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/, { name: 'object id' });
export const selectSchema = Joi.string().trim();
export const sortSchema = Joi.string().trim();
export const textSchema = Joi.string().trim();
export const numberSchema = Joi.number();
export const integerSchema = Joi.number().integer();
export const isoDateSchema = Joi.date().iso();
export const dateSchema = Joi.date();
export const booleanSchema = Joi.boolean();
export const passwordSchema = Joi.string().min(8);
// export const passwordSchema = Joi.string().regex(
//   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#^(),\.\/;`~ ])[A-Za-z\d@$!%*?&#^(),\.\/;`~ ]{8,}$/
// );

export const emailSchema = Joi.string().email({ minDomainSegments: 2 }).trim();
export const mongoIdSchema = Joi.string()
  .regex(/[a-fA-F0-9]{24}$/, { name: 'object id' })
  .trim();
export const arraySchema = Joi.array();

export const baseFindSchema = {
  pageSize: pageSizeSchema.default(30).max(60),
  page: pageSchema.min(1).default(1),
  select: selectSchema,
  sort: sortSchema,
  populate: textSchema
};
