import { PopulateOptions, QueryOptions, Schema } from 'mongoose';

export interface IBaseQuery {
  pageSize?: number;
  page?: number;
  sort?: string;
  select?: string;
  populate?: string;
}

export interface ILeanBaseQuery {
  page?: number;
  populate?: any;
}

export interface ICustomQueryOptions extends Omit<QueryOptions, 'populate'> {
  page?: number;
  populate?: PopulateOptions | PopulateOptions[];
}

export type TCustomObjectId = string | Schema.Types.ObjectId | ObjectConstructor;
export interface IAppleIAP {
  quantity: string;
  product_id: string;
  amount: number;
  vndAmount: number;
  transaction_id: string;
  original_transaction_id: string;
  purchase_date: string;
  purchase_date_ms: string;
  purchase_date_pst: string;
  original_purchase_date: string;
  original_purchase_date_ms: string;
  original_purchase_date_pst: string;
  is_trial_period: string;
  in_app_ownership_type: string;
}

export interface IAndroidIAPVerify {
  token: string;
  productId: string;
}
export interface IAndroidIAP {
  purchaseTimeMillis?: string;
  amount: number;
  vndAmount: number;
  purchaseState?: number;
  consumptionState?: number;
  developerPayload?: string;
  orderId?: string;
  purchaseType?: number;
  acknowledgementState?: number;
  kind?: string;
  regionCode?: string;
}
