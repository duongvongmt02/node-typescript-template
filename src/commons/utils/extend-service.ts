import { bindAll } from 'lodash';
import {
  Model,
  Document,
  LeanDocument,
  FilterQuery,
  QueryOptions,
  SaveOptions,
  InsertManyOptions,
  DocumentDefinition
} from 'mongoose';
import { CollectionBulkWriteOptions } from 'mongodb';
import { ICustomQueryOptions } from './base-interface';

export function extendService<T extends Document>(model: Model<T>) {
  bindAll(model, [
    'create',
    'findOne',
    'find',
    'findById',
    'findOneAndUpdate',
    'findOneAndDelete',
    'countDocuments',
    'updateOne',
    'updateMany',
    'update',
    'aggregate',
    'findByIdAndUpdate'
  ]);

  const findWithPagination = (
    query: FilterQuery<T>,
    select: any,
    options: ICustomQueryOptions
  ): Promise<[T[], { total: number; totalPage?: number }]> => {
    const { limit = 20, page = 1, ...otherOptions } = options;
    const newOptions = otherOptions as any;
    if (newOptions.populate) {
      const populate = newOptions.populate as any;
      newOptions.populate = populate;
    }
    if (!newOptions.sort) {
      newOptions.sort = '-createdAt';
    }
    return Promise.all([
      model.find(query, select, { skip: page && (page - 1) * limit, limit, ...newOptions }),
      model.countDocuments(query).then((res) => ({ total: res, totalPage: Math.ceil(res / limit) }))
    ]);
  };

  const create = (doc: LeanDocument<T>, options?: SaveOptions): Promise<Document<T>> => {
    return new model(doc).save(options);
  };

  const find = (query: FilterQuery<T>, select?: any, options: QueryOptions = {}) => {
    if (!options.sort) {
      options.sort = '-createdAt';
    }
    return model.find(query, select, options);
  };
  const bulkWrite = (writes: any[], options?: CollectionBulkWriteOptions) => {
    return model.bulkWrite(writes, options);
  };
  const insertMany = (docs: T[] | DocumentDefinition<T>[], options?: InsertManyOptions) => {
    return model.insertMany(docs, options);
  };
  const methods = {
    create,
    findOne: model.findOne,
    find,
    findById: model.findById,
    findOneAndUpdate: model.findOneAndUpdate,
    findOneAndDelete: model.findOneAndDelete,
    countDocuments: model.countDocuments,
    updateOne: model.updateOne,
    updateMany: model.updateMany,
    findByIdAndUpdate: model.findByIdAndUpdate,
    update: model.update,
    aggregate: model.aggregate,
    generate: (doc: object): T => new model(doc),
    findWithPagination,
    bulkWrite,
    insertMany
  };
  return methods;
}
