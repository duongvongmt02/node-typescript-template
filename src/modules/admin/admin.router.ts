import { Router } from 'express';
import { isAdminAuthorized, validate } from '../../commons';
import { adminController } from './admin.controller';
import { adminValidation } from './admin.validation';

const router = Router();
const adminRouter = Router();

adminRouter.get('/', validate('query', adminValidation.admin.get), adminController.admin.getList);
adminRouter.post(
  '/change-password',
  validate('body', adminValidation.admin.changePassword),
  adminController.admin.changePassword
);

router.use('/admin', isAdminAuthorized, adminRouter);

export { router as adminRouter };
