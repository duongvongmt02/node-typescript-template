import { Document } from 'mongoose';
import { IBaseQuery } from '../../commons';

export interface IConfession {
  title: string;
  description: string;
  email?: string;
  status: TConfessionStatus;
  reason?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IConfessionCreating {
  title: string;
  description: string;
  email?: string;
}

export interface IConfessionQuery extends IBaseQuery {
  status?: TConfessionStatus;
  email?: string;
}

export interface IConfessionConfirm {
  status: TConfessionStatus;
  reason: string;
}

export interface IConfessionSendConfirmedToMail {
  status: TConfessionStatus;
  email: string;
  reason: string;
  title: string;
}

export type TConfessionStatus = 'pending' | 'approved' | 'cancelled';

export interface IConfessionDocument extends IConfession, Document {}
