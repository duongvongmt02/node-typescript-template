import { Router } from 'express';
import { isAdminAuthorized, isUserAuthorized, validateMany } from '../../commons/middlewares';
import { confessionValidation } from './confession.validation';
import { confessionController } from './confession.controller';

const router = Router();
const userRouter = Router();
const adminRouter = Router();

router
  .post('/', validateMany(confessionValidation.any.create), confessionController.any.create)
  .get(
    '/get-list',
    validateMany(confessionValidation.any.getList),
    confessionController.any.getList
  )
  .get(
    '/:_id',
    validateMany(confessionValidation.any.getDetail),
    confessionController.any.getDetail
  );

adminRouter
  .put(
    '/confirm/:_id',
    validateMany(confessionValidation.admin.confirm),
    confessionController.admin.confirm
  )
  .delete(
    '/:_id',
    validateMany(confessionValidation.admin.delete),
    confessionController.admin.deleteConfession
  );

router.use('/user', isUserAuthorized, userRouter);
router.use('/admin', isAdminAuthorized, adminRouter);
export { router as confessionRouter };
