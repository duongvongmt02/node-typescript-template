import { Model, model, Schema } from 'mongoose';
import { confessionStatus } from './confession.config';
import { IConfession, IConfessionDocument } from './confession.interface';

const confessionSchema = new Schema<IConfessionDocument, Model<IConfessionDocument>, IConfession>(
  {
    title: String,
    description: String,
    email: String,
    status: { type: String, enum: Object.values(confessionStatus) },
    reason: String
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

export default model<IConfessionDocument>('confession', confessionSchema);
