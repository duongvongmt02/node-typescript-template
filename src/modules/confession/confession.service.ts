import confessionModel from './confession.model';
import { extendService, logger } from '../../commons/utils';
import { IConfessionSendConfirmedToMail } from './confession.interface';
import fs from 'fs';
import { sendMail } from '../../commons/utils/send-mail';

const sendConfirmedToMail = (data: IConfessionSendConfirmedToMail) => {
  const statusDescription = {
    approved: 'đã được duyệt!',
    cancelled: 'đã bị từ chối!',
    pending: ''
  };
  const { email, reason, status, title } = data;
  const descriptionStatus = statusDescription[status];

  const reasonDescription =
    status === 'cancelled'
      ? `Bài viết ${title} của bạn bị từ chối vì ` + reason
      : `Chúng tôi vô cùng vui mừng thông báo rằng bài viết ${title} của bạn đã được duyệt và đăng lên diễn đàn của chúng tối!`;

  const userName = email.substr(0, email.indexOf('@'));
  let html = fs.readFileSync('assets/confessionConfirmed.html', 'utf-8');
  html = html.replace(/{{userName}}/g, userName);
  html = html.replace(/{{confirmDescription}}/g, reasonDescription);
  html = html.replace(/{{status}}/g, descriptionStatus);
  sendMail(email, 'Đã duyệt bài viết', html).catch((error) => {
    logger.error(error);
  });
};

export const confessionService = {
  ...extendService(confessionModel),
  sendConfirmedToMail
};
