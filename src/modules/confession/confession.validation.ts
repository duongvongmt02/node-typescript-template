import Joi from 'joi';
import {
  emailSchema,
  integerSchema,
  mongoIdSchema,
  numberSchema,
  stringSchema
} from '../../commons';
import { confessionStatus } from './confession.config';
import {
  IConfession,
  IConfessionConfirm,
  IConfessionCreating,
  IConfessionQuery
} from './confession.interface';

export const confessionValidation = {
  any: {
    create: {
      body: Joi.object<IConfessionCreating>().keys({
        description: stringSchema.empty({ allow: false }).required(),
        title: stringSchema.empty({ allow: false }).required(),
        email: emailSchema
      })
    },
    getList: {
      query: Joi.object<IConfessionQuery>().keys({
        email: Joi.string().email({ minDomainSegments: 2 }),
        page: integerSchema.min(1).required(),
        pageSize: integerSchema.min(1).required(),
        status: stringSchema.valid(...Object.values(confessionStatus)),
        sort: stringSchema
      })
    },
    getDetail: {
      params: Joi.object().keys({
        _id: mongoIdSchema.required()
      })
    }
  },
  user: {},
  admin: {
    confirm: {
      body: Joi.object<IConfessionConfirm>().keys({
        status: stringSchema.valid(...Object.values(confessionStatus)).required(),
        reason: stringSchema.when('status', {
          is: confessionStatus.cancelled,
          then: Joi.required(),
          otherwise: Joi.forbidden()
        })
      }),
      params: Joi.object().keys({
        _id: mongoIdSchema.required()
      })
    },
    delete: {
      params: Joi.object().keys({
        _id: mongoIdSchema.required()
      })
    }
  },
  superAdmin: {},
  mod: {},
  superMod: {}
};
