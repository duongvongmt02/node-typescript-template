import { TConfessionStatus } from './confession.interface';

export const confessionStatus: Record<TConfessionStatus, TConfessionStatus> = {
  approved: 'approved',
  cancelled: 'cancelled',
  pending: 'pending'
};
