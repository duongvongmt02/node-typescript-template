import { BaseError, BaseResponse, errors as Errors } from '../../commons/utils';
import { confessionStatus } from './confession.config';
import { IConfessionCreating, IConfessionQuery } from './confession.interface';
import { confessionService } from './confession.service';

export const confessionController = {
  any: {
    getList: async (req: any, res: any, next: any) => {
      try {
        const { page, pageSize, select, sort, populate, ...query }: IConfessionQuery = req.query;
        const [confessionList, { total }] = await confessionService.findWithPagination(
          query,
          select,
          {
            page,
            limit: pageSize,
            sort
          }
        );
        return new BaseResponse({ statusCode: 200, data: confessionList })
          .addMeta({ total })
          .return(res);
      } catch (error) {
        return next(error);
      }
    },
    getDetail: async (req: any, res: any, next: any) => {
      try {
        const { _id } = req.params;
        const confession = await confessionService.findById(_id);
        return new BaseResponse({ statusCode: 200, data: confession }).return(res);
      } catch (error) {
        return next(error);
      }
    },
    create: async (req: any, res: any, next: any) => {
      try {
        const { title, description, email }: IConfessionCreating = req.body;

        const confession = await confessionService.create({
          title,
          description,
          email,
          status: confessionStatus.pending
        });
        return new BaseResponse({ statusCode: 200, data: confession }).return(res);
      } catch (error) {
        return next(error);
      }
    }
  },
  user: {},
  admin: {
    confirm: async (req: any, res: any, next: any) => {
      try {
        const { _id } = req.params;
        const { status, reason } = req.body;
        const confession = await confessionService.findOne({
          _id,
          status: confessionStatus.pending
          // status: { $ne: confessionStatus.cancelled }
        });
        if (!confession) {
          return new BaseError({
            statusCode: 400,
            message: 'Bài viết không tồn tại',
            messageCode: Errors.confession.notFound
          }).return(res);
        }
        confession.status = status;
        confession.reason = reason;
        if (confession.email) {
          confessionService.sendConfirmedToMail({
            email: confession.email,
            reason,
            status,
            title: confession.title
          });
        }
        await confession.save();
        return new BaseResponse({ statusCode: 200, data: confession }).return(res);
      } catch (error) {
        return next(error);
      }
    },
    deleteConfession: async (req: any, res: any, next: any) => {
      try {
        const { _id } = req.params;
        const confession = await confessionService.findOneAndDelete({ _id });
        return new BaseResponse({ statusCode: 200, data: confession }).return(res);
      } catch (error) {
        return next(error);
      }
    }
  },
  superAdmin: {},
  mod: {},
  superMod: {}
};
