import { Router } from 'express';
import { isUserAuthorized, validate } from '../../commons';
import { userController } from './user.controller';
import { userValidation } from './user.validation';

const router = Router();
const userRouter = Router();

userRouter.get('/info', userController.user.getUserInfo);
userRouter.post(
  '/change-password',
  validate('body', userValidation.user.changePassword),
  userController.user.changePassword
);

router.use('/user', isUserAuthorized, userRouter);

export { router as userRouter };
