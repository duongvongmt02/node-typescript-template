import { Document } from 'mongoose';

export interface I_Module_ {
  createdAt?: Date;
  updatedAt?: Date;
}

export interface I_Module_Document extends I_Module_, Document {}
