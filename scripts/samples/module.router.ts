import { Router } from 'express';
import { isAdminAuthorized, isAdminPermission, isUserAuthorized } from '../../commons/middlewares';
import { _moDule_Validation } from './_module_.validation';
import { _moDule_Controller } from './_module_.controller';

const router = Router();
const userRouter = Router();
const adminRouter = Router();
const superAdminRouter = Router();
const modRouter = Router();
const superModRouter = Router();

router.use('/user', isUserAuthorized, userRouter);
router.use('/admin', isAdminAuthorized, isAdminPermission(['admin']), adminRouter);
router.use('/superAdmin', isAdminAuthorized, isAdminPermission(['superAdmin']), superAdminRouter);
router.use('/mod', isAdminAuthorized, isAdminPermission(['mod', 'admin', 'superAdmin']), modRouter);
router.use('/superMod', isAdminPermission(['admin', 'superMod', 'superAdmin']), superModRouter);
export { router as _moDule_Router };
