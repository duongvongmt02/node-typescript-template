import { Model, model, Schema } from 'mongoose';
import { I_Module_, I_Module_Document } from './_module_.interface';

const _moDule_Schema = new Schema<I_Module_Document, Model<I_Module_Document>, I_Module_>(
  {},
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

export default model<I_Module_Document>('_mo_dule_', _moDule_Schema);
