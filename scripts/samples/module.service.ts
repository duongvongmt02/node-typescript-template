import _moDule_Model from './_module_.model';
import { extendService } from '../../commons/utils';

export const _moDule_Service = {
  ...extendService(_moDule_Model)
};
