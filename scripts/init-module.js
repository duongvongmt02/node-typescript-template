const fs = require('fs');
const path = require('path');
const replace = require('replace-in-file');

const InitFileTypes = [
  'config.ts',
  'interface.ts',
  'controller.ts',
  'router.ts',
  'service.ts',
  'model.ts',
  'validation.ts'
];

(async () => {
  try {
    const moduleName = process.argv[2].toLowerCase();
    const modulePath = path.resolve(__dirname, '../src', 'modules', moduleName);

    const _parts_ = moduleName.split('-');
    const _module_ = moduleName;
    const _moDule_ =
      _parts_.length > 1
        ? _parts_[0] +
          _parts_.slice(1).reduce((acc, cur) => acc + cur[0].toUpperCase() + cur.slice(1), '')
        : _parts_[0];
    const _mo_dule_ = _module_.replace(/-/g, '_');
    const _Module_ = _moDule_[0].toUpperCase() + _moDule_.slice(1);
    const _MODULE_ = _Module_.toUpperCase();

    if (!fs.existsSync(modulePath)) {
      fs.mkdirSync(modulePath);
      console.log(`[${modulePath}] is created`);
    }

    for (const fileType of InitFileTypes) {
      if (!fs.existsSync(path.resolve(modulePath, `${moduleName}.${fileType}`))) {
        const srcFile = path.resolve(__dirname, 'samples', `module.${fileType}`);
        const dstFile = path.resolve(modulePath, `${moduleName}.${fileType}`);
        fs.copyFileSync(srcFile, dstFile);
        await replace({ files: dstFile, from: /_module_/g, to: _module_ });
        await replace({ files: dstFile, from: /_moDule_/g, to: _moDule_ });
        await replace({ files: dstFile, from: /_mo_dule_/g, to: _mo_dule_ });
        await replace({ files: dstFile, from: /_Module_/g, to: _Module_ });
      }
    }

    if (!fs.existsSync(path.resolve(modulePath, 'index.ts'))) {
      const srcFile = path.resolve(__dirname, 'samples', 'index.ts');
      const dstFile = path.resolve(modulePath, 'index.ts');
      fs.copyFileSync(srcFile, dstFile);
      await replace({ files: dstFile, from: /_module_/g, to: _module_ });
      await replace({ files: dstFile, from: /_moDule_/g, to: _moDule_ });
      await replace({ files: dstFile, from: /_mo_dule_/g, to: _mo_dule_ });
      await replace({ files: dstFile, from: /_Module_/g, to: _Module_ });
    }

    console.log('done!');
  } catch (err) {
    console.error(err);
  } finally {
    process.exit(0);
  }
})();
